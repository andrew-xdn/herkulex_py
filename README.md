# Simple herkulex protocol library (WIP)

## Installation

`sudo apt install python3-serial python3-pip`

`pip3 install prettytable`

Set your serial port in `config.py` file.

## Examples

Read status (ping): `./status.py`

Read EEP registers: `./read_eep_reg.py`

Read RAM registers: `./read_ram_reg.py`

Enable servo torque: `./torque_on.py`

Disable servo torque: `./torque_off.py`

Move servos (JOG): `./move_servo.py`

Reset servo: `./reset.py`

