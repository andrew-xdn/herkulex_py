#!/usr/bin/env python
# -*- coding: utf-8 -*-
from array import array
#from struct import *
#import pprint

# Stop flag
STOP_FLAG        = 0b00000001

# JOG Mode
POSITION_CONTROL = 0b00000000
INFINITE_TURN    = 0b00000010

# Green, Blue, Red color codes
LED_OFF          = 0b00000000
LED_WHITE        = 0b00011100
LED_RED          = 0b00010000
LED_GREEN        = 0b00000100
LED_BLUE         = 0b00001000
LED_YELLOW       = 0b00010100
LED_CYAN         = 0b00001100
LED_MAGNETA      = 0b00011000

# JOG Invalid
JOG_INVALID      = 0b00100000

SIG_INFINITE_TURN= 0x4000

class Jog(object):
  ''' Base Jog class '''

  def __init__(self):
    self._jogs = {}

  def add(self, servo_id, position, param=0):
    if servo_id < 0 or servo_id > 0xFE:
      raise Exception("Wrong servo id")
    #if position < 0 or position > 1024:
    #  raise Exception("Wrong position %d" % position)
    if param < 0 or param > 0xFE:
      raise Exception("Wrong set param")
    if param & INFINITE_TURN == INFINITE_TURN:
      position = position | SIG_INFINITE_TURN
    self._jogs[servo_id] = {'pos':position, 'set':param}
    return self
  
  def _get(self, servo_id):
    jog = self._jogs[servo_id]
    return [ (jog['pos'] % 256), (jog['pos'] >> 8), jog['set'], servo_id ]

  def tolist(self):
    rez = []
    st = ''
    for servo_id in self._jogs:
      rez.extend( self._get(servo_id) )
    return rez


class IJog(Jog):
  '''I_Jog can set the operation timing of individual servo [Pg 40,45,48]'''
  def add(self, servo_id, position, playtime, param=0):
     #if playtime < 0 or playtime > 0xFE:
     # raise Exception("Wrong playtime")
     Jog.add(self, servo_id, position, param)
     self._jogs[servo_id]['time'] = playtime
     return self
  
  def _get(self, servo_id):
    jog = self._jogs[servo_id]
    return [ (jog['pos'] % 256), (jog['pos'] >> 8), jog['set'], servo_id, jog['time'] ]


class SJog(Jog):
  '''S_Jog All servo operate simulationaly at the same time [Pg 40,46,48]'''
  def __init__(self, playtime):
    self._playtime = 60
    self._jogs = {}
    if playtime < 0 or playtime > 0xFE:
      raise Exception("Wrong servo id")
    self._playtime = playtime

  def tolist(self):
    rez = Jog.tolist(self)
    rez.insert(0, self._playtime)
    return rez

'''
i = IJog()
i.add(253, 512, 0xfe, LED_RED ).add(10, 320, 0xfe, INFINITE_TURN | LED_BLUE )
s = SJog(0xfe)
s.add(253, 512, LED_RED ).add(10, 320, INFINITE_TURN | LED_BLUE )

out = i.tolist()
out = array('B', out).tostring()
print(hexdump(out, 'i = '))

out = s.tolist()
out = array('B', out).tostring()
print(hexdump(out, 's = '))
#'''

