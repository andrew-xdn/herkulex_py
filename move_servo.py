#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from herkulex.io import *
import sys

servo = 0xfe # JOG must be broadcast
cmd = 6 # sjog command

jog = SJog(60)
# add(<id>, <pos>, <params>)
position = int(sys.argv[1])
jog.add(100, position, LED_MAGNETA)
jog.add(101, position, LED_MAGNETA)
jog.add(102, position, LED_MAGNETA)
jog.add(103, position, LED_MAGNETA)
print( hexdump(write_packet(servo, cmd, jog.tolist() ), '<< ' ) ) # S_JOG

