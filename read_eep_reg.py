#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from herkulex.io import *

servo = 100 # servo id
cmd = 2 # EEP read command
addr = 0 # EEP reg addr begin
ln = 54 # EEP reg len (max 54)

inp = write_packet(servo, cmd, (addr, ln) )   # EEP_READ
print(hexdump(inp, "<<"))

# let's wait point one second before reading output (let's give device time to answer)
time.sleep(0.1)
out = read_packet(servo, cmd, addr, ln)
print( hexdump(out, ">>"))

data = out[7:] # cut header

if data != []:
    print( Registers(cmd, data) )
else:
    print("Servo is not responding")

