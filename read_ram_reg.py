#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from herkulex.io import *

servo = 100 # servo id
cmd = 4 # ram read command
addr = 0 # ram reg addr begin
ln = 74 # ram reg len (max 74)

inp = write_packet(servo, cmd, (addr, ln) )   # RAM_READ
print(hexdump(inp, "<<"))

# let's wait point one second before reading output (let's give device time to answer)
time.sleep(0.1)

try:
  out = read_packet(servo, cmd, addr, ln)
except e:
  pass

print( hexdump(out, ">>"))

data = out[7:] # cut header

if data != []:
    print( Registers(cmd, data) )
else:
    print("Servo is not responding")
