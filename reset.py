#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from config import *
from herkulex.io import *

servo = 100 # servo id
cmd = 9 # reset command

inp = write_packet(servo, cmd )
print(hexdump(inp, "<<"))

