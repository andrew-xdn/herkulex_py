#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from config import *
from herkulex.io import *

servo = 100 # servo id
cmd = 7 # status command

inp = write_packet(servo, cmd )
print(hexdump(inp, "<<"))

# let's wait point one second before reading output (let's give device time to answer)
time.sleep(0.1)
out = read_packet(servo, cmd)
print( hexdump(out, ">>"))

data = out[7:] # cut header

if data != []:
    print( Registers(cmd, data) )
else:
    print("Servo is not responding")

