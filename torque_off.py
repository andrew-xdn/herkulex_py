#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from herkulex.io import *
import sys

servo = int(sys.argv[1]) # servo id
cmd = 3 # ram write command
addr = 52 # ram reg addr begin
ln = 1 # ram reg len (max 74)

# 0x00 = torque off
# 0x60 = torque on
value = 0x00 

inp = write_packet(servo, cmd, (addr, ln, value) ) # RAM_WRITE
print(hexdump(inp, "<<"))

